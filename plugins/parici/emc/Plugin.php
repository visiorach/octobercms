<?php namespace ParIci\EMC;

use Backend;
use System\Classes\PluginBase;

/**
 * EMC Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'EMC',
            'description' => 'No description provided yet...',
            'author'      => 'ParIci',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'ParIci\EMC\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        //return []; // Remove this line to activate

        return [
            'parici.emc.some_permission' => [
                'tab' => 'EMC',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'emc' => [
                'label'       => 'EMC',
                'url'         => Backend::url('parici/emc/mot'),
                'icon'        => 'icon-leaf',
                'permissions' => ['parici.emc.*'],
                'order'       => 100,
				'sideMenu' => [
					'emc' => [
						'label'       => 'EMC',
						'url'         => Backend::url('parici/emc/mot'),
						'icon'        => 'icon-leaf',
						'permissions' => ['parici.emc.*'],
						'order'       => 100,
					],
					'cycle' => [
						'label'       => 'Cycles',
						'url'         => Backend::url('parici/emc/cycle'),
						'icon'        => 'icon-leaf',
						'permissions' => ['parici.emc.*'],
						'order'       => 100,
					],
					'annee' => [
						'label'       => 'Années',
						'url'         => Backend::url('parici/emc/annee'),
						'icon'        => 'icon-leaf',
						'permissions' => ['parici.emc.*'],
						'order'       => 100,
					],
				]
            ],
        ];
    }
}
