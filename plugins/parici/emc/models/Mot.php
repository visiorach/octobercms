<?php namespace ParIci\EMC\Models;

use Model;

/**
 * mot Model
 */
class Mot extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'parici_emc_mots';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [	
      
	];
    public $belongsTo = [];
    public $belongsToMany = [
		"cycles" => "ParIci\EMC\Models\Cycle",
		"annees"  => "ParIci\EMC\Models\Annee"
	];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
		'audio' => \System\Models\File::class ,
		'audio_definition' => \System\Models\File::class ,
		'audio_example' => \System\Models\File::class 
	];

    public $attachMany = [];
}
