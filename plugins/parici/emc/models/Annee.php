<?php namespace ParIci\EMC\Models;

use Model;

/**
 * annee Model
 */
class Annee extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'parici_emc_annees';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
