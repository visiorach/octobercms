<?php namespace ParIci\EMC\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMotsTable extends Migration
{
    public function up()
    {
        Schema::create('parici_emc_mots', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nom');
            $table->string('phonetique');
            $table->string('nature');
            $table->string('definition');
            $table->text('example');
            $table->boolean('is_homophone');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('parici_emc_mots');
    }
}
