<?php namespace ParIci\EMC\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMotAnneesTable extends Migration
{
    public function up()
    {
        Schema::create('annee_mot', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer("mot_id")->unsigned();
			$table->integer("annee_id")->unsigned();
			$table->primary(['mot_id', 'annee_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('parici_emc_annee_mots');
    }
}
