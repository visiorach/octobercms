<?php namespace ParIci\EMC\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMotCyclesTable extends Migration
{
    public function up()
    {
        Schema::create('cycle_mot', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer("mot_id")->unsigned();
			$table->integer("cycle_id")->unsigned();
			$table->primary(['mot_id', 'cycle_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('parici_emc_cycle_mots');
    }
}
