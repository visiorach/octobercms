<?php namespace ParIci\EMC\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCyclesTable extends Migration
{
    public function up()
    {
        Schema::create('parici_emc_cycles', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('nom');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('parici_emc_cycles');
    }
}
