<?php namespace ParIci\EMC\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Annee Back-end Controller
 */
class Annee extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('ParIci.EMC', 'emc', 'annee');
    }
}
