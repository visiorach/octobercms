<?php namespace ParIci\EMC\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Cycle Back-end Controller
 */
class Cycle extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('ParIci.EMC', 'emc', 'cycle');
    }
}
